var fs = require("fs/promises");

async function main() {
    const fileName = process.argv[2];
    const outFileName = process.argv[3];
    const file = await fs.readFile(fileName, {
        'encoding': 'utf-8'
    });

    const processedFile = file.replace(/regeneratorRuntime/g, "window.JB_EXTENSIONS.regeneratorRuntime");    
    await fs.writeFile(outFileName, processedFile);
}

main().catch(e => console.error(e));