(function(window) {
    const BASE_URL = "http://localhost:10021";
    const SECRET = "P-cFAB5VDDJfyy"; // modify this for your own to complicate heuristics analysis

    async function getItem(key) {
        const res = await fetch(
            `${BASE_URL}/get?key=${key}`,
            {
                method: "GET",
                headers: {
                    "x-secret": SECRET,
                }
            }
        );

        const json = await res.json();
        return !json.keyExists ? undefined : json.value;
    }

    async function setItem(key, value) {
        const res = await fetch(
            `${BASE_URL}/set`,
            {
                method: "POST",
                headers: {
                    "x-secret": SECRET,
                },
                body: JSON.stringify({
                    key,
                    value
                })
            }
        );

        const json = await res.json();
        return !!json.success;
    }

    async function removeItem(key) {
        const res = await fetch(
            `${BASE_URL}/remove`,
            {
                method: "POST",
                headers: {
                    "x-secret": SECRET,
                },
                body: JSON.stringify({
                    key
                })
            }
        );

        const json = await res.json();
        return !!json.success;
    }

    if (!window.JB_EXTENSIONS) {
        window.JB_EXTENSIONS = {};
    }

    window.JB_EXTENSIONS.globalStorage = {
        getItem,
        setItem,
        removeItem,
    };
})(window);
